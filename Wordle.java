/*Student ID 2035715
*Assignement3
*Create a world game
*Date 12/06/2023
*/ 
import java.util.Random;
import java.util.Scanner;
public class Wordle {
    /*//Main class
    //Only calls generateWord method
    //And runGame method
    public static void main(String[] args){
        String answer = generateWord();
		System.out.println(answer);
        runGame(answer);
        }*/

    //Create an array of random words with no repeated letter
    //Choosing random word from an array
    //Return chossen word
    public static String generateWord(){
        String[] arrayAnswers = {"blind", "axiom" , "guard", "shade", "force", "shame", "sting", "float", "brave", "faith", "flash", "plant", "brain", "vocal", "choke", "split", "alien", "enjoy", "rebus", "irony"};
        Random answer = new Random();
        int randomIndex = answer.nextInt(arrayAnswers.length);
        String chosenAnswer = arrayAnswers[randomIndex].toUpperCase();
    return chosenAnswer;
    }
    //checking presence of a certain letter in word
    //Return true if letter is in word or false if letter is not in word
    public static boolean letterInWord(String answer, char letter){
        for(int i=0; i<answer.length(); i++){
            if(letter>=65 && letter<=90  && letter == answer.charAt(i)){
                return true;
            }
            else if(letter-32 == answer.charAt(i)){
                 return true;
            }
        }
        return false;
    }
    // Check if place of a letter is in word
    // Return true if place is right or false if letter is in wrong spot 
    public static boolean letterInSlot(String answer, char letter, int position){
        for(int i=0; i<answer.length(); i++){
            if(letter>=65 && letter<=90  && letter == answer.charAt(position)){
                return true;
            }
            else if(letter-32 == answer.charAt(position)){
                 return true;
            }
        }
        return false;
    }
    //  Create array of colors
    // Green is letterInSlot == true & letterInWord == true
    // Yellow if letter in word == true
    // White if both false
    //return colorful array
    public static String[] guessWord(String answer, String guess){
        String[] arrayColors = {"white", "yellow", "green"};
        String[] arrayColorsGuess = new String[answer.length()];
        for(int i=0; i<answer.length(); i++){
            if(letterInWord(answer, guess.charAt(i)) && letterInSlot(answer, guess.charAt(i), i)){
                arrayColorsGuess[i] = arrayColors[2];
            }
            else if(letterInWord(answer, guess.charAt(i)) && letterInSlot(answer, guess.charAt(i), i)!= true){       
                arrayColorsGuess[i] = arrayColors[1];
            }
            else{
                arrayColorsGuess[i] = arrayColors[0];
            }
        }
            return arrayColorsGuess;
    }
    // Color all the letters
    // Print the word (ech letter with the right color) using array of colors
    public static void presentResults(String guess, String[] arrayColorsGuess){
        String colorGreen  = "\u001B[32m";
        String colorYellow = "\u001B[33m";
        String colorWhite  = "\u001B[0m";
        for(int i=0; i<guess.length(); i++){
            if(arrayColorsGuess[i] == "green"){
                System.out.print(colorGreen + guess.charAt(i) + colorWhite);
            }
            else if(arrayColorsGuess[i] == "yellow"){
                System.out.print(colorYellow + guess.charAt(i) + colorWhite);
            }
            else{
                System.out.print(colorWhite + guess.charAt(i) + colorWhite);
            }
        }
        System.out.println();
    }
    // Asks user to input w5 letter word
    // Retrun guessed word
    public static String readGuess(){
        Scanner reader = new Scanner(System.in);
        System.out.println("Enter a 5-letter word with no letters repeated: ");
        String guess = reader.next();
        while(guess.length() != 5){
        System.out.println("Enter a 5-letter word with no letters repeated: ");
        guess = reader.next();
        }
        if(guess.length() != 5){
            System.out.println("Please enter a 5 letter word: ");
        }
        return guess;
    }
    // Method calls other methods to check each letter
    // Counts attempts
    // Forcing user t input 5 letter word
    // Check if person won or lost
    public static void runGame(String answer){
        System.out.println("Let's start the worlde game!");
        int count = 0;
            for(int i=6; i>0; i--){
            String guess = readGuess();
            String[] arrayColorsGuess = guessWord(answer, guess);
            if(answer.toUpperCase().equals(guess.toUpperCase()) && i!=0){
                presentResults(guess, arrayColorsGuess);
                System.out.println("You won!");
                break;
            }
            else if((!answer.toUpperCase().equals(guess.toUpperCase())) && i!=0){
                System.out.println("You have " + (i-1) + " attempts left!");
                presentResults(guess, arrayColorsGuess);
                count++;
            }
        }
        if(count == 6 ){
            System.out.println("The right word was: " + answer);
            System.out.println("You lost! Try again!");
        }
    }
}