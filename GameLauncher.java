 import java.util.Scanner;

 public class GameLauncher{
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		System.out.println("Hello! Today we'll play a game!");
		System.out.println("Enter 1 gor Hangman or 2 for Wordle");
		
		int choice = reader.nextInt();
		if(choice ==1){
			System.out.print("Enter a word for the Hangman game (4 different letters): ");
			String word = reader.next().toUpperCase();
			Hangman.runGame(word);
		}
		else if(choice ==2){
			String answer = Wordle.generateWord();
			System.out.println(answer);
			Wordle.runGame(answer);
		}
	}
 }