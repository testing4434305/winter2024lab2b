import java.util.Scanner;

public class Hangman {
    
	/*//ask for a word
	//call rungame
     public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a word for the Hangman game (4 different letters): ");
		
		//The word is toUppercase to avoid extra lines in other methods
        String word = reader.next().toUpperCase();
		
		//System.out.println(word);
		//Comment printing the word, so the game is fair
		
		runGame(word);
    }*/
    //call toUpperCase method for c and chars in word
    public static int isLetterInWord(String word, char c) {
		for(int i=0; i<word.length(); i++){
			if (word.charAt(i) == c) {
            return i;
            }
		}
		return -1;
    }

     // make chars from word and c uppercase with -32
    public static char toUpperCase(char c) {
        if(c >= 97 && c<=122){
            c -= 32;
            return c;
        }
        else{
            return c;
        }
    }
	
	
	//use if and else for loop
	//check every position in word from 0 - 3
	//return letter in a right place or just _
	//print the word again

    public static void printWork(String word, boolean[] letters) {
        
		String result = "Your result is ";
        int position = 0;
		for(int i = 0; i<word.length(); i++){
			if(letters[i]){
				result += word.charAt(i) + " ";
			}
			else{
				result += "_ ";
			}
			position ++;
		}
		System.out.println(result);
    }
	
	
	//ask for a letter
	//check for position 
	// create attempts with max of 6
	//print lost or won
	public static void runGame(String word){
		boolean[] letters = {false, false, false, false};
		int attempt = 6;
		
		for(int i=0; i>=0; i++){
			System.out.println("Enter a letter: ");
            Scanner reader = new Scanner(System.in);
            char guess = reader.next().charAt(0);
            guess = toUpperCase(guess);
			int position = isLetterInWord(word, guess);
			
			if(position != -1){
				letters[position] = true;
				System.out.println("Attempts: " + attempt);
			}
			else {
				attempt--;
				System.out.println("This letter is not in the word!");
                System.out.println("Attempts: " + attempt);
			}
			printWork(word, letters);
			
			int count=0;
			for(boolean value:letters){
				if(value){
					count++;
				}
			}
			if (attempt <= 0) {
            System.out.println("You lost. The word was: " + word);
			break;
			}
			else if(count==4){
				System.out.println("Congratulations! You guessed the word: " + word);
				break;
			}
			else{
				System.out.println("Keep guessing");
			}
			System.out.println();
		}
	}
}
